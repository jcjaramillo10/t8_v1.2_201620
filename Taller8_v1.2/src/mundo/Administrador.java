package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String, Arco<String,String>> grafo;

	
	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<String, Arco<String,String>>();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		ArrayList<String> nuevo = new ArrayList<String>();
		Estacion<String> buscar = (Estacion<String>) grafo.buscarNodo(identificador);
		for(int i=0; i<buscar.getRutas().length; i++){
			if(buscar.getRutas()[i] != null)
			nuevo.add(buscar.getRutas()[i].getLlave());
		}
		
		String[] lista = new String[nuevo.size()];
		for (int i=0; i<lista.length;i++){
			lista[i] = nuevo.get(i);
		}	
		if (lista.length != 0)	return lista;
		else return null;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public String distanciaMinimaEstaciones()
	{
		//TODO Implementar
		double distancia = Double.POSITIVE_INFINITY;
		String inicio = "";
		String fin = "";
		Arco<String, Arco<String, String>>[] arcos = grafo.darArcos();
		for(int i=0; i<arcos.length;i++){
			if (arcos[i].darCosto() < distancia){
				distancia = arcos[i].darCosto();
				inicio = arcos[i].darNodoInicio().darId();
				fin = arcos[i].darNodoFin().darId();
			}
		}
		return inicio + " y " + fin + " a " +distancia;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public String  distanciaMaximaEstaciones()
	{
		//TODO Implementar
		double distancia = 0.0;
		String inicio = "";
		String fin = "";
		Arco<String, Arco<String, String>>[] arcos = grafo.darArcos();
		for(int i=0; i<arcos.length;i++){
			if (arcos[i].darCosto() > distancia){
				distancia = arcos[i].darCosto();
				inicio = arcos[i].darNodoInicio().darId();
				fin = arcos[i].darNodoFin().darId();
			}
		}
		return inicio + " y " + fin + " a " +distancia;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{

		BufferedReader paraderos = new BufferedReader(new FileReader(new File(RUTA_PARADEROS)));
		BufferedReader ruta = new BufferedReader(new FileReader(new File(RUTA_RUTAS)));
		
		int total = Integer.parseInt(paraderos.readLine());
		grafo.dimensionarNodos(total+10);
		String linea = paraderos.readLine();
		while (linea!=null){
			String[] datos = linea.split(";");
			grafo.agregarNodo(new Estacion(Double.parseDouble(datos[1]),Double.parseDouble(datos[2]),datos[0]));
			linea = paraderos.readLine();
		}
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		//TODO Implementar
		int totalRutas = Integer.parseInt(ruta.readLine());
		grafo.dimensionarArcos(total*3);
		String separador = "--------------------------------------";
		String nombre = "";
		int j = 0;
		int cantidadParaderos = 0;
		String actual = ruta.readLine();
		while (j <= totalRutas){
			if(!actual.equals(separador)){
				nombre = actual;
				cantidadParaderos = Integer.parseInt(ruta.readLine());
				int k = 0;
				String inicio = "";
				String fin = ruta.readLine();
				while (k<cantidadParaderos){
					String[] datoI = inicio.split(" ");
					String[] datoF = fin.split(" ");
					String idI = datoI[0];
					String idF = datoF[0];
					double costoF = Double.parseDouble(datoF[1]);
					Estacion<String> nodoF = (Estacion<String>) grafo.buscarNodo(idF);
					nodoF.agregarRutas(nombre,costoF);
					if (!idI.equals(""))
						grafo.agregarArco(idI, idF, costoF);
					inicio = fin;
					fin = ruta.readLine();
					k++;
				}
			}
			actual = ruta.readLine();
			j++;
		}
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
		paraderos.close();
		ruta.close();
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		return (Estacion<String>) grafo.buscarNodo(identificador);
	}

}
