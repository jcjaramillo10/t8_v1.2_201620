package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.Estacion;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos
 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	
	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private TablaHashLineal<K, Nodo<K>> nodos;

	/**
	 * Lista de adyacencia 
	 */
	private TablaHashLineal<K, Arco<K, E>> arcos;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		nodos = new TablaHashLineal<K, Nodo<K>>();
		arcos = new TablaHashLineal<K,Arco<K,E>>();
	}
	
	public void dimensionarNodos(int capacidad){
		nodos = new TablaHashLineal<K, Nodo<K>>(capacidad, (float) 1.0);
	}

	public void dimensionarArcos(int capacidad){
		arcos = new TablaHashLineal<K, Arco<K,E>>(capacidad, (float) 1.0);
	}
	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
			nodos.put(nodo.darId(), nodo);
			return true;
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		Nodo<K> yo = nodos.get(id);
		if (yo == null)
			return false;
		else{
			nodos.put(id, null);
			return true;
		}
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		ArrayList<Arco<K,E>> nuevo = new ArrayList<Arco<K,E> >();
		
		for (int i=0; i<arcos.size(); i++){
			if(arcos.darTablaTotal()[i] != null)
			nuevo.add(arcos.darTablaTotal()[i].getValor());
		}
		
		Arco<K,E>[] lista = new Arco[nuevo.size()];
		for (int i=0; i<lista.length;i++){
			lista[i] = nuevo.get(i);
		}	
		return lista;
	}
	/**
	 * Crea un arco guardado en una tabla hash, con su llave la llave del
	 * nodo de inicio y su valor el arco creado.
	 * @param inicio Llave del nodo de inicio
	 * @param fin Llave del nodo final
	 * @param costo costo entre las llaves 
	 * @param e Objeto adicional
	 * @return arco. Arco creado
	 */
	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			Arco<K,E> nuevo = new Arco<K,E>( nodoI, nodoF, costo, e);
			arcos.put(inicio, nuevo);
			return nuevo;
		}
		else
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		ArrayList<Nodo<K>> nuevo = new ArrayList<Nodo<K> >();
		
		for (int i=0; i<nodos.size(); i++){
			if(nodos.darTablaTotal()[i] != null)
				nuevo.add(nodos.darTablaTotal()[i].getValor());
		}
		
		Nodo<K>[] lista = new Nodo[nuevo.size()];
		for (int i=0; i<lista.length;i++){
			lista[i] = nuevo.get(i);
		}	
		return lista;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		
		return crearArco(inicio, fin, costo, obj) != null;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		return nodos.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		ArrayList<Arco<K,E>> nuevo = new ArrayList<Arco<K,E> >();
		Nodo<K> nodoI = buscarNodo(id);
		for (int i=0; i<arcos.size(); i++){
			Arco<K, E> actual = arcos.darTablaTotal()[i].getValor();
			if (nodoI.equals(actual.darNodoInicio()))
				nuevo.add(actual);
		}
		
		Arco<K,E>[] lista = new Arco[nuevo.size()];
		for (int i=0; i<lista.length;i++){
			lista[i] = nuevo.get(i);
		}	
		return lista;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
		ArrayList<Arco<K,E>> nuevo = new ArrayList<Arco<K,E> >();
		Nodo<K> nodoF = buscarNodo(id);
		for (int i=0; i<arcos.size(); i++){
			Arco<K, E> actual = arcos.darTablaTotal()[i].getValor();
			if (nodoF.equals(actual.darNodoInicio()))
				nuevo.add(actual);
		}
		
		Arco<K,E>[] lista = new Arco[nuevo.size()];
		for (int i=0; i<lista.length;i++){
			lista[i] = nuevo.get(i);
		}	
		return lista;
	}

}
